import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class GameController {

    private final ExecutorService executorService;
    private final Game game;
    private final StringProperty userInput;

    public GameController(Game game) {
        this.game = game;
        executorService = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }
        });
        userInput = new SimpleStringProperty("");
    }

    @FXML
    private VBox board ;
    @FXML
    private Label statusLabel ;
    @FXML
    private Label enterALetterLabel ;
    @FXML
    private Label userInputLabel ;
    @FXML
    private TextField textField ;

    public void initialize() throws IOException {
        System.out.println("in initialize");
        drawHangman();
        addTextBoxListener();
        setUpStatusLabelBindings();
        setUpUserInputLabelBindings();
    }

    private void addTextBoxListener() {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.length() > 0) {
                System.out.print(newValue);
                game.makeMove(newValue);
                userInput.set(userInput.get() + newValue);
                textField.clear();
            }
        });
    }

    private void setUpStatusLabelBindings() {

        System.out.println("in setUpStatusLabelBindings");
        statusLabel.textProperty().bind(Bindings.format("%s", game.gameStatusProperty()));
        enterALetterLabel.textProperty().bind(Bindings.format("%s", "Enter a letter:"));
    }

    private void setUpUserInputLabelBindings() {
        userInputLabel.textProperty().bind(Bindings.format("User input: %s", userInput));
    }

    private void drawHangman() {

        Line base = new Line(50, 200, 150, 200);
        base.setStrokeWidth(5);
        base.setStroke(Color.BLACK);

        Line post = new Line(100, 200, 100, 50);
        post.setStrokeWidth(5);
        post.setStroke(Color.BLACK);

        Line topBar = new Line(100, 50, 200, 50);
        topBar.setStrokeWidth(5);
        topBar.setStroke(Color.BLACK);

        Line rope = new Line(200, 50, 200, 70);
        rope.setStrokeWidth(3);
        rope.setStroke(Color.BLACK);

        board.getChildren().addAll(base, post, topBar, rope);

    }

    @FXML
    private void newHangman() {
        game.reset();
        userInput.set("");
    }

    @FXML
    private void quit() {
        board.getScene().getWindow().hide();
    }
}
